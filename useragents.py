input_file = open('input.txt', 'r')

num_line = 0
num_win = 0
num_ubuntu = 0
num_mac = 0
num_unknown = 0

for line in input_file:
    num_line += 1
    parse_line = line.split('"')
    user_agent = parse_line[5].split('(')
    if 'Windows' in parse_line[5]:
        num_win += 1
    elif 'Macintosh' in parse_line[5]:
        num_mac += 1
    elif 'Ubuntu' in parse_line[5]:
        num_ubuntu += 1
    else:
        num_unknown += 1

dic = {"Windows:": num_win, "OS X:": num_mac, "Ubuntu:": num_ubuntu,
       "Unknown:": num_unknown}
for system, num in sorted(dic.items(), key=lambda x: x[1]):
    print(system, num)
