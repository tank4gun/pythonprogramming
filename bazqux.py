n = [i for i in range(1, 101)]
print(' '.join(map(str, n)))
for i in range(3, 101, 3):
    n[i - 1] = 'Baz'
for i in range(5, 101, 5):
    n[i - 1] = 'Qux'
for i in range(15, 101, 15):
    n[i - 1] = 'BazQux'
print(' '.join(map(str, n)))
