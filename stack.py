import sys


class Stack:
    def __init__(self, obj):
        self.st = []
        for i in obj:
            self.st.append(i)

    def push(self, obj):
        self.st.append(obj)

    def top(self):
        return self.st[-1]

    def pop(self):
        obj = self.st[-1]
        self.st.pop()
        return obj

    def __len__(self):
        return len(self.st)

    def __str__(self):
        ans = ''
        for i in range(0, len(self.st) - 1):
            ans += str(self.st[i]) + ' '
        ans += str(self.st[-1])
        return ans

exec(sys.stdin.read())
