input_file = open('input.txt', 'r')
num_req = 0
num_err1 = 0
num_err2 = 0
num_err3 = 0
for line in input_file:
    num_req += 1
    parse_line = line.split('"')
    ans_code, ans_len = parse_line[2].split(' ')[1:3]
    if int(ans_code) == 200:
        num_err1 += 1
    elif int(ans_code) >= 300 and int(ans_code) <= 309:
        num_err2 += 1
    else:
        num_err3 += 1
print(num_err1, num_err2, num_err3, num_req, sep="\n")
