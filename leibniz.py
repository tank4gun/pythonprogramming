ans = 0
for i in range(1, 21, 2):
    if (i - 1) % 4 == 0:
        ans += (1 / i)
    else:
        ans -= (1 / i)
ans *= 4
print(ans)
