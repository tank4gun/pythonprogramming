import sys


class ComplexNumber:
    def __init__(self, real=0, imaginary=0):
        self.re = real
        self.im = imaginary

    def __add__(self, other):
        return ComplexNumber(self.re + other.re, self.im + other.im)

    def __sub__(self, other):
        return ComplexNumber(self.re - other.re, self.im - other.im)

    def __mul__(self, other):
        re = self.re * other.re - self.im * other.im
        im = self.re * other.im + self.im * other.re
        return ComplexNumber(re, im)

    def __truediv__(self, other):
        denominator = other.re ** 2 + other.im ** 2
        re = (self.re * other.re + self.im * other.im) / denominator
        im = (self.im * other.re - self.re * other.im) / denominator
        return ComplexNumber(re, im)

    def __str__(self):
        ans = ''
        if self.re != 0:
            ans += "%.2f" % self.re
            if self.im != 0:
                if self.im < 0:
                    ans += " - %.2fi" % abs(self.im)
                else:
                    ans += " + %.2fi" % self.im
        else:
            if self.im != 0:
                ans += "%.2fi" % self.im
            else:
                ans += "0.00"
        return ans

if __name__ == "__main__":
    for line in sys.stdin.readlines():
        print(eval(line.strip()))
