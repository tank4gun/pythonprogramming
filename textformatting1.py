import sys
import argparse

def text_to_paragraphs(text):
    paragraph_mass = text.split('\n')
    new_par_mass = []
    ind = 0
    new_par_flag = False

    while ind < len(paragraph_mass) and paragraph_mass[ind] == '':
        ind += 1

    if ind == len(paragraph_mass) - 1:
        new_par_mass.append('')
        return new_par_mass

    while ind < len(paragraph_mass):
        if paragraph_mass[ind] != '':
            new_par_flag = False
            new_par_mass.append(paragraph_mass[ind])
            ind += 1
        else:
            if new_par_flag:
                ind += 1
            else:
                new_par_mass.append('\t')
                new_par_flag = True
                ind += 1

    cur_string = ''
    final_par = []
    for line in new_par_mass:
        if line != '\t':
            cur_string += line
            cur_string += ' '
        else:
            final_par.append(cur_string)
            cur_string = ''
            final_par.append(line)

    return final_par


def fine_string(string):
    new_string = ''
    dictionary = [chr(44), chr(46), chr(63), chr(33),
                  chr(45), chr(58), chr(39)]
    is_on_word = True
    looking_for_punct_mark = False

    for symb in string:
        if is_on_word:
            if symb.isalnum():
                new_string += symb
            elif symb in dictionary:
                is_on_word = False
                looking_for_punct_mark = True
                new_string += symb
            elif symb == ' ':
                is_on_word = False
                looking_for_punct_mark = True
        elif looking_for_punct_mark:
            if symb.isalnum():
                is_on_word = True
                new_string += ' '
                new_string += symb
                looking_for_punct_mark = False
            elif symb in dictionary:
                new_string += symb

    return new_string


def lines_to_words(text):
    new_text = []
    for line in text:
        if line != '\t':
            new_text.append(fine_string(line))
        else:
            new_text.append('\t')
    return new_text


def read_text(text, input_file):
    for line in input_file:
        text += line
    return text


def find_longest_word(text):
    max_len = 0
    for line in text:
        words = line.split(' ')
        for word in words:
            if len(word) > max_len:
                max_len = len(word)
    return max_len


def write_to_file(text, output, whitespaces, len_line):
    new_par = True
    tab = ' ' * whitespaces
    cur_len_string = 0

    for line in text:
        if line == '\t':
            new_par = True
            output.write('\n')
            cur_len_string = 0
        else:
            if new_par:
                output.write(tab)
                cur_len_string += whitespaces
                words = line.split(' ')
                new_par = False
                if len(words[0]) + cur_len_string > len_line:
                    output.write('\n')
                    cur_len_string = 0

                for i in range(0, len(words)):
                    output.write(words[i])
                    cur_len_string += len(words[i])
                    if i != len(words) - 1:
                        if cur_len_string + len(words[i + 1]) + 1 < len_line:
                            output.write(' ')
                            cur_len_string += 1
                        else:
                            output.write('\n')
                            cur_len_string = 0


parser = argparse.ArgumentParser()
parser.add_argument('--input', '-i', type=open, default=sys.stdin,
                    help='file, which should be formatted')
parser.add_argument('--output', '-o', type=argparse.FileType('w'),
                    default=sys.stdout,
                    help='file, where we write the final text')
parser.add_argument('--line-length', '-l', type=int, required=True,
                    help='the maximum lenght of string in final text')
parser.add_argument('--paragraph-spaces', '-p', type=int, required=True,
                    help='the number of whitespaces before new paragraph')
args = parser.parse_args()

text = args.input.read()
text = lines_to_words(text_to_paragraphs(text))
max_len = find_longest_word(text)

if max_len > args.line_length:
    raise ValueError("Some words are longer, than string max size!")

write_to_file(text, args.output, args.paragraph_spaces, args.line_length)

